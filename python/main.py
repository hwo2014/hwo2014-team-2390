import json
import socket
import sys

class Driver(object):
    
    alone = None

    def __init__(self):
        pass

class Track(object):

    def __init__(self, track_data):
        self.track_data = track_data
        print(self.track_data)

class Car(object):

    car_color = None
    track = None

    turbo_available = False
    turbo_duration = None
    turbo_factor = None

    def __init__(self, driver, socket, name, key):
        self.driver = driver
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def turbo(self, are_you_sure):
        if are_you_sure and self.turbo_available:
            self.turbo_available = False
            self.msg("turbo", "VROoOOOOoOOOooOOooOoOOOM!!")
        elif not self.turbo_available:
            print("Turbo not available")
        else:
            pass #do nothing

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_your_car(self, data):
        print("Your car is {0}".format(data['color']))
        self.car_color = data['color']
        self.ping()

    def on_game_init(self, data):
        print("Game init")
        self.track = Track(data['race']['track'])
        self.ping()

    def on_game_start(self, data):
        print("Game start")
        self.ping()

    def on_car_positions(self, data):
        self.throttle(0.6)

    def on_crash(self, data):
        if data['color'] == self.car_color:
            print("You crashed")
        else:
            print("{0} crashed".format(data['color']))
        self.ping()

    def on_spawn(self, data):
        if data['color'] == self.car_color:
            print("You spawned")
        else:
            print("{0} spawned".format(data['color']))
        self.ping()

    def on_turbo_available(self, data):
        print("Turbo available")
        self.turbo_available = True
        self.turbo_duration = float(data['turboDurationMilliseconds'])
        self.turbo_factor = float(data['turboFactor'])

    def on_lap_finished(self, data):
        if data['car']['color'] == self.car_color:
            print("Lap finished with time {0}".format(data['lapTime']['millis']))
        else:
            print("Someone finished a lap")
        self.ping()

    def on_finish(self, data):
        if data['color'] == self.car_color:
            print("You finished")

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_tournament_end(self, data):
        print("Tournament ended")
        self.ping()

    def on_dnf(self, data):
        print("{0} was disqualified".format(data['car']['color']))
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car,
            'carPositions': self.on_car_positions,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'turboAvailable': self.on_turbo_available,
            'lapFinished': self.on_lap_finished,
            'finish': self.on_finish,
            'gameEnd': self.on_game_end,
            'tournamentEnd': self.on_tournament_end,
            'dnf': self.on_dnf,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Car(Driver(), s, name, key)
        bot.run()
